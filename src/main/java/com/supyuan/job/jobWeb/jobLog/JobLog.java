package com.supyuan.job.jobWeb.jobLog;


import com.extjfinal.base.BaseModel;
import com.extjfinal.component.annotation.ModelBind;

/**
 * Created by yuanxuyun on 2017/4/21.
 */
@ModelBind(table = "job_log", key = "uids")
public class JobLog extends BaseModel<JobLog> {
    public static final JobLog dao = new JobLog();
}
